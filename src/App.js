// import logo from './logo.svg';
import  Auth from './pages/auth/Auth.js';
import  Dashboard from './pages/dashboard/Dashboard.js';
import  Kategori from './pages/kategori/Kategori.js';
import  Transaksi from './pages/transaksi/Transaksi.js';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'


function App() {
  return (
    <div className="App container-bg">
      {/* <header className="App-header">
        aaaaa
      </header> */}
      <Router>
        <Routes>
          <Route path="/" element={<ToAuth />} />
          <Route path="/dashboard" element={<ToDashboard />} />
          <Route path="/kategori" element={<ToKategori />} />
          <Route path="/transaksi" element={<ToTransaksi />} />
        </Routes>
      </Router>
    </div>
  );
}

const ToAuth = () => {
  return <Auth></Auth>;
};

const ToDashboard = () => {
  return <Dashboard></Dashboard>;
};

const ToKategori = () => {
  return <Kategori></Kategori>;
};
const ToTransaksi = () => {
  return <Transaksi></Transaksi>;
};
export default App;
