import React,  { useEffect,useState } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDollarSign,faCircleUp, faCircleDown } from '@fortawesome/free-solid-svg-icons';
import Sidebar from '../components/Sidebar';
import { useNavigate } from 'react-router-dom';



const Dashboard = () => {


    const navigate = useNavigate();

    const [data, setData] = useState({
        saldo: '',
        pengeluaran: '',
        pemasukan: '',
    });

    // import css
    useEffect(() => {
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = '/css/style.css';
        document.head.appendChild(link);
        
        const sidebar = document.createElement('link');
        sidebar.rel = 'stylesheet';
        sidebar.href = '/css/sidebar.css';
        document.head.appendChild(sidebar);

        const apiUrl = process.env.REACT_APP_API_URL;

        const config = {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`,
            },
        };
    
        axios.get(apiUrl+"/saldo",config)
        .then(response => {
            setData(response.data.data);
        })
        .catch(error => {
        if (error.response) {
            if (error.response.status === 401) {
                navigate('/');
            } else {
                console.error("Kesalahan lain:", error);
            }
        } else {
            console.error("Kesalahan lain:", error);
        }
        });

        return () => {
            document.head.removeChild(link);
            document.head.removeChild(sidebar);
        };

    }, [navigate]);

    return (
        <div className="wrapper d-flex align-items-stretch">
        <Sidebar />
        <div id="content" className="p-4 p-md-5 pt-5">
            <h2 className="mb-4">Dashboard</h2>
            <div className="row px-2">
                <div className="col-md-4 px-2 d-flex align-items-stretch">
                    <div className="w-100 h-100 bg-primary rounded shadow-lg px-3 py-4">
                        <div className="row">
                            <div className="col-md-5 text-white text-center">
                                <FontAwesomeIcon className="icon-dashbord" icon={faDollarSign}/>
                            </div>
                            <div className="col-md-7">
                                <h3 className="text-light text-center">TOTAL SALDO <br/> <h1 className="text-white"><b>{data.saldo}</b></h1></h3>                        
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 px-2 d-flex align-items-stretch">
                    <div className="w-100 h-100 bg-success rounded shadow-lg px-3 py-4">
                        <div className="row">
                            <div className="col-md-5 text-white text-center">
                                <FontAwesomeIcon className="icon-dashbord" icon={faCircleUp}/>
                            </div>
                            <div className="col-md-7">
                                <h3 className="text-light text-center">PEMASUKAN <br/> <h1 className="text-white"><b> {data.pemasukan}</b></h1></h3>                        
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 px-2 d-flex align-items-stretch">
                    <div className="w-100 h-100 bg-danger rounded shadow-lg px-3 py-4">
                        <div className="row">
                            <div className="col-md-5 text-white text-center">
                                <FontAwesomeIcon className="icon-dashbord" icon={faCircleDown}/>
                            </div>
                            <div className="col-md-7">
                                <h3 className="text-light text-center">PENGELUARAN <br/> <h1 className="text-white"><b> {data.pengeluaran}</b></h1></h3>                        
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        </div>
    );
};

export default Dashboard;