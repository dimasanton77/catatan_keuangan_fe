import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faCog, faSignOut, faMoneyBillTransfer } from '@fortawesome/free-solid-svg-icons';
import Swal from 'sweetalert2';
import axios from 'axios';
import { useNavigate,Link,useLocation } from 'react-router-dom';


const Sidebar = () => {

    const navigate = useNavigate();
    const location = useLocation();
    const apiUrl = process.env.REACT_APP_API_URL;

    const logOut = (e) => {
        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: "Untuk mengakhiri sesi ini",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yakin',
            cancelButtonText: 'Tidak'
          }).then((result) => {
            if (result.isConfirmed) {

                const headers = {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                  };
                
                axios.post(apiUrl+"/logout", {}, {headers})
                .then(response => {
                    Swal.fire({
                        icon: 'success',
                        title: 'Sukses!',
                        text: response.data.message,
                    });
                    setTimeout(() => {
                        navigate('/');
                    }, 1500);
                })
                .catch(error => {
                if (error.response) {
                    if (error.response.status === 401) {
                        navigate('/');
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Gagal!',
                            text: error.response.data.message,
                        });
                    }
                } else {
                    console.error("Kesalahan lain:", error);
                }
                });
            }
          })
    };

    return (
    <nav id="sidebar">
        <div className="custom-menu">
        <button type="button" id="sidebarCollapse" className="btn btn-primary">
            <span className="fa fa-bars"></span>
        </button>
        </div>
        <div className="img bg-wrap text-center py-4" style={{ backgroundImage: "url(https://runsystem.id/wp-content/uploads/2022/02/thoughtful-businessman-sitting-with-open-laptop-computer-looking-worried-while-thinking-about-planning-top-view-e1645670579227.jpg)" }}>
        <div className="user-logo">
            <h3>Manajemen <br/> Keuangan</h3>
        </div>
        </div>
        <ul className="list-unstyled components mb-5">
        <li className={`${location.pathname === '/dashboard' ? 'active' : ''}`}>
            <Link to="/dashboard">
                <div><FontAwesomeIcon icon={faHome} className="mr-3" /> Dashboard</div>
            </Link>
        </li>
        <li className={`${location.pathname === '/kategori' ? 'active' : ''}`}>
            <Link to="/kategori">
                <div><FontAwesomeIcon icon={faCog} className="mr-3" /> Kategori</div>
            </Link>
        </li>
        <li className={`${location.pathname === '/transaksi' ? 'active' : ''}`}>
            <Link to="/transaksi">
                <div><FontAwesomeIcon icon={faMoneyBillTransfer} className="mr-3" /> Transaksi</div>
            </Link>
        </li>
        <li onClick={logOut}>
            <Link to="#">
                <div><FontAwesomeIcon icon={faSignOut} className="mr-3" /> Sign Out</div>
            </Link>
        </li>
        </ul>
    </nav>
    );
};

export default Sidebar;
