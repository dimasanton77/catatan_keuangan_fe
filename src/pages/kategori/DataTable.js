import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Table, Form, Pagination, Button, Modal } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash,faEdit } from '@fortawesome/free-solid-svg-icons';


const DataTable = () => {

    // config
    const apiUrl = process.env.REACT_APP_API_URL;
    const navigate = useNavigate();
    const [isDataSaved, setIsDataSaved] = useState(false);
    
    // modal add
    const [showModal, setShowModal] = useState(false);
    const handleShowModal = () => {
        setShowModal(true);
    };
    const handleCloseModal = () => {
        setShowModal(false);
    };

    // modal edit
    const [showModalEdit, setShowModalEdit] = useState(false);
    const handleShowModalEdit = (id,nama,tipe) => {
        setShowModalEdit(true);
        setFormDataEdit({
            ...formDataEdit,
            nama: nama,
            tipe: tipe,
            id: id,
        });
    };
    const handleCloseModalEdit = () => {
        setShowModalEdit(false);
    };

    // delete
    const handleDelete = (id) => {
        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: "Untuk manghapus data ini",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yakin',
            cancelButtonText: 'Tidak'
          }).then((result) => {
            if (result.isConfirmed) {

                const headers = {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                  };
                
                axios.delete(apiUrl+"/kategori/"+id, {headers})
                .then(response => {
                    Swal.fire({
                        icon: 'success',
                        title: 'Sukses!',
                        text: response.data.message,
                    });
                    setIsDataSaved(true);
                })
                .catch(error => {
                if (error.response) {
                    if (error.response.status === 401) {
                        navigate('/');
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Gagal!',
                            text: error.response.data.message,
                        });
                    }
                } else {
                    console.error("Kesalahan lain:", error);
                }
                });
            }
          })
    };

    // add data
    const [formData, setFormData] = useState({
        nama: '',
        tipe: '',
    });
    const handleChangeAdd = (event) => {
        const { name, value } = event.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };
    const handleSubmitAdd = (event) => {
        event.preventDefault();
        const headers = {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
        };
        axios.post(apiUrl+"/kategori", formData, {headers})
        .then(response => {
            Swal.fire({
                icon: 'success',
                title: 'Sukses!',
                text: response.data.message,
            });
            setIsDataSaved(true);
        })
        .catch(error => {
        if (error.response) {
            if (error.response.status === 401) {
                navigate('/');
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal!',
                    text: error.response.data.message,
                });
            }
        } else {
            console.error("Kesalahan lain:", error);
        }
        });
    };


    // edit data
    const [formDataEdit, setFormDataEdit] = useState({
        nama: '',
        tipe: '',
        id: '',
    });
    const handleChangeEdit = (event) => {
        const { name, value } = event.target;
        setFormDataEdit({
            ...formDataEdit,
            [name]: value,
        });
    };
    const handleSubmitEdit = (event) => {
        event.preventDefault();
        const headers = {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
        };
        axios.put(apiUrl+"/kategori/"+formDataEdit.id, formDataEdit, {headers})
        .then(response => {
            setIsDataSaved(true);
            Swal.fire({
                icon: 'success',
                title: 'Sukses!',
                text: response.data.message,
            });
        })
        .catch(error => {
        if (error.response) {
            if (error.response.status === 401) {
                navigate('/');
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal!',
                    text: error.response.data.message,
                });
            }
        } else {
            console.error("Kesalahan lain:", error);
        }
        });
    };

    // data table
    const [data, setData] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [itemsPerPage] = useState(5);
    const [searchTerm, setSearchTerm] = useState('');

    useEffect(() => {
    
    const apiUrl = process.env.REACT_APP_API_URL;
    const config = {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem("token")}`,
        },
    };

    axios.get(apiUrl+"/kategori",config)
        .then(response => {
            setData(response.data.data);
        })
        .catch(error => {
        if (error.response) {
            if (error.response.status === 401) {
                navigate('/');
            } else {
                console.error("Kesalahan lain:", error);
            }
        } else {
            console.error("Kesalahan lain:", error);
        }
        });
        setIsDataSaved(false);
    }, [isDataSaved,navigate]);


    useEffect(() => {

    const filtered = data.filter(item =>
        item.nama.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setFilteredData(filtered);
    }, [data, searchTerm]);

    const handleSearchChange = event => {
    setSearchTerm(event.target.value);
    };

    const handlePageChange = page => {
    setCurrentPage(page);
    };

    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = filteredData.slice(indexOfFirstItem, indexOfLastItem);

    return (
    <div className='w-100'>
    
        {/* Modal Add */}
        <Modal show={showModal} onHide={handleCloseModal}>
            <form onSubmit={handleSubmitAdd}>
                <Modal.Header closeButton>
                <Modal.Title>Tambah Kategori</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <label htmlFor="nama">Nama Kategori</label>
                        <input
                        type="text"
                        id="nama"
                        name="nama"
                        value={formData.nama}
                        onChange={handleChangeAdd}
                        required
                        className='form-control form'
                        />
                    </div>

                    <div className='mt-3'>
                        <label htmlFor="tipe">Tipe</label>
                        <select
                        id="tipe"
                        name="tipe"
                        value={formData.tipe}
                        onChange={handleChangeAdd}
                        required
                        className='form-control form'
                        >
                        <option value="">-- Pilih Tipe --</option>
                        <option value="pemasukan">Pemasukan</option>
                        <option value="pengeluaran">Pengeluaran</option>
                        </select>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseModal}>
                    Batal
                </Button>
                <Button variant="primary" onClick={handleCloseModal} type='submit'>
                    Simpan
                </Button>
                </Modal.Footer>
            </form>
        </Modal>

        {/* Modal Edit */}
        <Modal show={showModalEdit} onHide={handleCloseModalEdit}>
            <form onSubmit={handleSubmitEdit}>
                <Modal.Header closeButton>
                <Modal.Title>Edit Kategori</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <label htmlFor="nama">Nama Kategori</label>
                        <input
                        type="text"
                        id="nama"
                        name="nama"
                        value={formDataEdit.nama}
                        onChange={handleChangeEdit}
                        required
                        className='form-control form'
                        />
                    </div>

                    <div className='mt-3'>
                        <label htmlFor="tipe">Tipe</label>
                        <select
                        id="tipe"
                        name="tipe"
                        value={formDataEdit.tipe}
                        onChange={handleChangeEdit}
                        required
                        className='form-control form'
                        >
                        <option value="">-- Pilih Tipe --</option>
                        <option value="pemasukan">Pemasukan</option>
                        <option value="pengeluaran">Pengeluaran</option>
                        </select>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseModalEdit}>
                    Batal
                </Button>
                <Button variant="primary" onClick={handleCloseModalEdit} type='submit'>
                    Simpan
                </Button>
                </Modal.Footer>
            </form>
        </Modal>
        
        {/* Table */}
        <div className='w-100'>
            <div className='row'>
                <div className='col-9'>
                <Button variant="primary" onClick={handleShowModal}>
                    Tambah Kategori
                </Button>
                </div>
                <div className='col-3'>
                    <Form.Control
                    type="text"
                    placeholder="Search by Name"
                    value={searchTerm}
                    onChange={handleSearchChange}
                    className='filter'
                    />
                </div>
            </div>
            <br/>
            <Table className='w-100' striped bordered hover>
            <thead>
                <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Tipe</th>
                <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                {currentItems.map((item, index) => (
                <tr key={item.id}>
                    <td>{indexOfFirstItem + index + 1}</td>
                    <td>{item.nama}</td>
                    <td>{item.tipe}</td>
                    <td>
                        <Button variant="info" onClick={handleShowModalEdit.bind(null, item.id,item.nama,item.tipe)} className='mr-2' type='submit'>
                            <FontAwesomeIcon icon={faEdit}/> 
                        </Button>
                        <Button variant="danger" onClick={handleDelete.bind(null, item.id)} type='submit'>
                            <FontAwesomeIcon icon={faTrash}/> 
                        </Button>
                    </td>
                </tr>
                ))}
            </tbody>
            </Table>
            <div className='text-center'>
                <Pagination className='pg'>
                    {Array.from({ length: Math.ceil(filteredData.length / itemsPerPage) }).map((_, index) => (
                        <Pagination.Item key={index + 1} active={index + 1 === currentPage} onClick={() => handlePageChange(index + 1)}>
                        {index + 1}
                        </Pagination.Item>
                    ))}
                </Pagination>
            </div>
        </div>
    </div>

);
};

export default DataTable;
