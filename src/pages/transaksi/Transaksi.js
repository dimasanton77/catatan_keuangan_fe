import React,  { useEffect } from 'react';
// import axios from 'axios';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faDollarSign,faCircleUp, faCircleDown } from '@fortawesome/free-solid-svg-icons';
import Sidebar from '../components/Sidebar';
import DataTable from './DataTable';
// import { useNavigate } from 'react-router-dom';



const Transaksi = () => {


    // const navigate = useNavigate();


    // import css
    useEffect(() => {
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = '/css/style.css';
        document.head.appendChild(link);
        
        const sidebar = document.createElement('link');
        sidebar.rel = 'stylesheet';
        sidebar.href = '/css/sidebar.css';
        document.head.appendChild(sidebar);

        return () => {
            document.head.removeChild(link);
            document.head.removeChild(sidebar);
        };

    }, []);


    return (
        <div className="wrapper d-flex align-items-stretch">
        <Sidebar />
        <div id="content" className="p-4 p-md-5 pt-5">
            <h2 className="mb-4">Transaksi</h2>
            <div className="row px-2">
                <DataTable/>
            </div>
        </div>
        </div>
    );
};

export default Transaksi;