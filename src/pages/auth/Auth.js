import React,  { useEffect, useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

const Auth = () => {

const navigate = useNavigate();
const apiUrl = process.env.REACT_APP_API_URL;

// import css
useEffect(() => {
    const link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = '/css/auth.css';
    document.head.appendChild(link);
    return () => {
        document.head.removeChild(link);
    };
}, []);


// login
const [formLogin, setFormLogin] = useState({
    email: '',
    password: '',
});
const handleLoginChange = (e) => {
    const { name, value } = e.target;
    setFormLogin((prevFormLogin) => ({
        ...prevFormLogin,
        [name]: value,
    }));
};

// register
const [formRegister, setFormRegister] = useState({
    name: '',
    email: '',
    password: '',
    password_confirmation: '',
});
const handleRegisterChange = (e) => {
    const { name, value } = e.target;
    setFormRegister((prevFormRegister) => ({
        ...prevFormRegister,
        [name]: value,
    }));
};

// Login Function
const handleLogin = async (e) => {
    e.preventDefault(); 
    try {
        axios.post(apiUrl+"/login", formLogin)
        .then(response => {
            localStorage.setItem('token', response.data.data.auth.token);
            Swal.fire({
                icon: 'success',
                title: 'Sukses!',
                text: response.data.message,
            });
            setTimeout(() => {
                navigate('/dashboard');
            }, 1500);
        })
        .catch(error => {
        if (error.response) {
            if (error.response.status === 401) {
                navigate('/');
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal!',
                    text: error.response.data.message,
                });
            }
        } else {
            console.error("Kesalahan lain:", error);
        }
        });
    } catch (error) {
        console.error('Login failed:', error);
    }
};

// Register Function
const handleRegister = async (e) => {
    e.preventDefault(); 
    try {
        axios.post(apiUrl+"/register", formRegister)
        .then(response => {
            Swal.fire({
                icon: 'success',
                title: 'Sukses!',
                text: response.data.message,
            });
            setTimeout(() => {
                window.location.reload();
            }, 1500);
        })
        .catch(error => {
        if (error.response) {
            if (error.response.status === 401) {
                window.location.href = '/';
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal!',
                    text: error.response.data.message,
                });
            }
        } else {
            console.error("Kesalahan lain:", error);
        }
        });
    } catch (error) {
        console.error('Login failed:', error);
    }
};

return (
    <div className="section">
    <div className="container">
        <div className="row full-height justify-content-center">
        <div className="col-12 text-center align-self-center py-5">
            <div className="section pb-5 pt-5 pt-sm-2 text-center">
            <h6 className="mb-0 pb-3">
                <span>Log In </span>
                <span>Sign Up</span>
            </h6>
            <input className="checkbox" type="checkbox" id="reg-log" name="reg-log" />
            <label htmlFor="reg-log"></label>
            <div className="card-3d-wrap mx-auto">
                <div className="card-3d-wrapper">
                <div className="card-front">
                    <div className="center-wrap">
                    <div className="section text-center">
                        <h4 className="mb-4 pb-3">Log In</h4>
                        <form onSubmit={handleLogin}>
                        <div className="form-group">
                        <input type="email" name="email" value={formLogin.email} required onChange={handleLoginChange} className="form-style" placeholder="Masukkan Email" id="logemail" autoComplete="off" />
                        <i className="input-icon uil uil-at"></i>
                        </div>
                        <div className="form-group mt-2">
                        <input type="password" name="password" value={formLogin.password} required onChange={handleLoginChange}  className="form-style" placeholder="Masukkan Password" id="logpass" autoComplete="off" />
                        <i className="input-icon uil uil-lock-alt"></i>
                        </div>
                        <button className="btn mt-4" >submit</button>
                        </form>
                        <p className="mb-0 mt-4 text-center">
                        </p>
                    </div>
                    </div>
                </div>
                <div className="card-back">
                    <div className="center-wrap">
                    <div className="section text-center">
                        <h4 className="mb-4 pb-3">Sign Up</h4>
                        <form onSubmit={handleRegister}>
                        <div className="form-group">
                        <input type="text" name="name" value={formRegister.name} required onChange={handleRegisterChange} className="form-style" placeholder="Masukkan Nama Lengkap" id="logname" autoComplete="off" />
                        <i className="input-icon uil uil-user"></i>
                        </div>
                        <div className="form-group mt-2">
                        <input type="email" name="email" value={formRegister.email} required onChange={handleRegisterChange} className="form-style" placeholder="Masukkan Email" id="logemail" autoComplete="off" />
                        <i className="input-icon uil uil-at"></i>
                        </div>
                        <div className="form-group mt-2">
                        <input type="password" name="password" value={formRegister.password} required onChange={handleRegisterChange} className="form-style" placeholder="Masukkan Password" id="logpass" autoComplete="off" />
                        <i className="input-icon uil uil-lock-alt"></i>
                        </div>
                        <div className="form-group mt-2">
                        <input type="password" name="password_confirmation" value={formRegister.password_confirmation} required onChange={handleRegisterChange} className="form-style" placeholder="Masukkan Konfirmasi Password" id="logpass2" autoComplete="off" />
                        <i className="input-icon uil uil-lock-alt"></i>
                        </div>
                        <button className="btn mt-4">submit</button>
                        </form>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
);
};

export default Auth;