const { createProxyMiddleware } = require('http-proxy-middleware');
const apiUrl = process.env.REACT_APP_API_URL;
module.exports = function (app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: apiUrl, // Ganti dengan URL API server yang sebenarnya
      changeOrigin: true,
    })
  );
};